package = "luajit-woothee"
version = "0.1-0"
source = {
	url = "",
}
description = {
	summary = "luajit-woothee",
	detailed = [[
		[woothee-c](https://github.com/kjdev/woothee-c) ffi library.
	]],
	homepage = "https://bitbucket.org/senanetworksinc/luajit-woothee",
	license = "MIT/X11"
}
dependencies = {
}
build = {
	type = "builtin",
	modules = {
		["luajit-woothee.cdef"] = "luajit-woothee/cdef.lua",
		["luajit-woothee.implement"] = "luajit-woothee/implement.lua",
		["luajit-woothee.init"] = "luajit-woothee/init.lua",
	},
}

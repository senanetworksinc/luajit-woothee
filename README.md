# luajit-woothee


[woothee-c](https://github.com/kjdev/woothee-c)のバインディングです。

こちらのソースがオリジナルです。

https://github.com/kjdev/woothee-c

# Install
`$ luarocks install luajit-woothee`

# Usage

```lua
local luajitwoothee = require "luajit-woothee"

local woothee = luajitwoothee.woothee_parse("Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 6.1; Trident/4.0)")

print(woothee.name)

print(woothee.category)

print(woothee.os)

print(woothee.version)

print(woothee.os_version)

print(woothee:woothee_browser_challenge_msie("Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 6.1; Trident/4.0)"))

print(woothee:woothee_browser_challenge_msie("Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36"))

woothee:woothee_delete()
```


# Revesion

* 2020/05/01 v0.1 release
